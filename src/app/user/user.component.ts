import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {User} from './user'
@Component({
  selector: 'my-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {

  @Output() deleteEvent=new EventEmitter<User>();
  @Output() editEvent=new EventEmitter<User>();

  isEdit:boolean=false;
  editButtonText='Edit';
  user:User;
  
  constructor() { }

  ngOnInit() {
  }
  sendDelete(){
    this.deleteEvent.emit(this.user);
  }
   toggleEdit(){
    this.isEdit=!this.isEdit;
    this.isEdit ? this.editButtonText='Save': this.editButtonText='Edit';
    if (!this.isEdit){
       //only in 'save' mode we operate the function of editing-by sending to the futher 
      this.editEvent.emit(this.user); //this.user will be here already updated
     }
    }
}
