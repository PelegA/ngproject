import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';
@Injectable()
export class ProductsService {

  productObservable;
  constructor(private af:AngularFire) { }

  getProducts(){
  this.productObservable=this.af.database.list('/product').map(
      products=>{//this is already an array
        products.map( //map on regular array
          product=>{
            product.catNames=[];
            for(var p in product.category){//because each user has an attr called "posts" in FireBase
              product.catNames.push(this.af.database.object('/category/'+p))
            }
         }
        );
        return products;
      }
    )
    return this.productObservable;
  }

  deleteProduct(product){
   let productKey=product.$key;
   this.af.database.object('/product/'+productKey).remove(); 
  }
}
