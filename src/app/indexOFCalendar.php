<?php

require __DIR__ . '/vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile('client_secret.json');
$client->addScope('https://www.googleapis.com/auth/calendar');

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  
  $client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Calendar($client);

  $event = new Google_Service_Calendar_Event(array( //הגדרת אירוע באמצעות מערך
  'summary' => 'question 5 Angular test',
  'location' => 'Jerusalem',
  'description' => 'new meeting inventation',
  'start' => array(
    'dateTime' => '2017-02-28T14:00:00-14:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'end' => array(
    'dateTime' => '2017-02-28T14:05:00-14:05',
    'timeZone' => 'America/Los_Angeles',
  ),
  'recurrence' => array(
    'RRULE:FREQ=DAILY;COUNT=1'
  ),
  'attendees' => array(
    array('email' => 'pelegam@post.jce.ac.il'),
    array('email' => 'pelegamon5@gmail.com'),
  ),
  'reminders' => array(
    'useDefault' => FALSE,
    'overrides' => array(
      array('method' => 'email', 'minutes' => 24 * 60),
      array('method' => 'popup', 'minutes' => 10),
    ),
  ),
));
  
$calendarId = 'primary';
$event = $service->events->insert($calendarId, $event);
printf('Event created: %s\n', $event->htmlLink);
  
} else {
  $redirect_uri =  'http://pelegam.myweb.jce.ac.il/calendar/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));

}


